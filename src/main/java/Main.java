import org.mapstruct.factory.Mappers;

public class Main {
    public static void main(String[] args) {
        PersonEntity entity = new PersonEntity("serdar", 38);
        SimpleMapper mapper = Mappers.getMapper(SimpleMapper.class);
        PersonDTO dto = mapper.toDTO(entity);
        System.out.println("DTO's name: " + dto.getName() + " DTO's age: " + dto.getAge());
    }
}
