import org.mapstruct.Mapper;

/*
* Mapstruct API contains functions that automatically map between two Java Beans.
* With MapStruct we only need to create the interface, and the library will automatically
* create a concrete implementation during compile time.
*
* Here, MapStruct is able to map our beans automatically because they have the same field names.
*
* We can trigger the MapStruct processing by executing an mvn package/install/deploy.
* This will generate the implementation class under /target/generated-sources/annotations/.
* */
@Mapper
public interface SimpleMapper {
    PersonDTO toDTO(PersonEntity entity);
    PersonEntity toEntity(PersonDTO dto);
}
