
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-20T19:55:48+0300",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_131 (Oracle Corporation)"
)
public class SimpleMapperImpl implements SimpleMapper {

    @Override
    public PersonDTO toDTO(PersonEntity entity) {
        if ( entity == null ) {
            return null;
        }

        PersonDTO personDTO = new PersonDTO();

        personDTO.setName( entity.getName() );
        personDTO.setAge( entity.getAge() );

        return personDTO;
    }

    @Override
    public PersonEntity toEntity(PersonDTO dto) {
        if ( dto == null ) {
            return null;
        }

        PersonEntity personEntity = new PersonEntity();

        personEntity.setName( dto.getName() );
        personEntity.setAge( dto.getAge() );

        return personEntity;
    }
}
